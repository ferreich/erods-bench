LIN_REG_MR_DIR=$PHOENIX_DIR/phoenix-2.0/tests/linear_regression
LIN_REG_MR_BIN=$LIN_REG_MR_DIR/linear_regression
LIN_REG_MR_INPUTS_ARCHIVE=$BENCH_PATH/tmp/lin_reg_input.tar.gz
LIN_REG_MR_INPUTS_DIR=$TMP_INPUTS_DIR/lin_reg

prepare_lin_reg_mr() {
  if [[ ! -f $LIN_REG_MR_INPUTS_DIR/word_100MB.txt ]]; then
      mkdir -p $LIN_REG_MR_INPUTS_DIR
      tar -xzf $LIN_REG_MR_INPUTS_ARCHIVE -C $LIN_REG_MR_INPUTS_DIR --strip-components 1 linear_regression_datafiles/key_file_500MB.txt
  fi

  if [ ! -f $LIN_REG_MR_INPUTS_DIR/input ]; then
      for i in $(seq 1 6); do
          cat $LIN_REG_MR_INPUTS_DIR/key_file_500MB.txt >> $LIN_REG_MR_INPUTS_DIR/input
      done
  fi
}

dump_lin_reg_mr() {
    sha1sum $LIN_REG_MR_BIN              >>$dump_program_dir/lin_reg_mr
    sha1sum $LIN_REG_MR_INPUTS_DIR/input >>$dump_program_dir/lin_reg_mr
}

command_lin_reg_mr() {
    echo MR_NUMPROCS=$threads MAPRED_NO_BINDING=1 $cmd_prefix $LIN_REG_MR_BIN $LIN_REG_MR_INPUTS_DIR/input
}
