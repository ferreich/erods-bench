export PCA_MR_DIR=$PHOENIX_DIR/phoenix-2.0/tests/pca
export PCA_MR_BIN=$PCA_MR_DIR/pca

prepare_pca_mr() {
    true
}

dump_pca_mr() {
    sha1sum $PCA_MR_BIN >>$dump_program_dir/pca_mr
}

command_pca_mr() {
    echo MR_NUMPROCS=$threads $cmd_prefix $PCA_MR_BIN -r 4000 -c 4000
}

