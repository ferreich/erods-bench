export VOLREND_DIR=$PARSEC_DIR/ext/splash2x/apps/volrend
export VOLREND_BIN=$VOLREND_DIR/inst/amd64-linux.gcc-pthreads/bin/volrend
export VOLREND_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/ext/splash2x/apps/volrend/inputs/input_native.tar
export VOLREND_INPUTS_DIR=$TMP_INPUTS_DIR/volrend
export VOLREND_ARG_SIZE=500

prepare_volrend() {
    if [[ ! -f $VOLREND_INPUTS_DIR/head.opc ]]; then
        mkdir -p $VOLREND_INPUTS_DIR
        tar -xf $VOLREND_INPUTS_ARCHIVE -C $VOLREND_INPUTS_DIR
    fi
}

dump_volrend() {
    sha1sum $VOLREND_BIN          >>$dump_program_dir/volrend
    sha1sum $VOLREND_INPUTS_DIR/* >>$dump_program_dir/volrend
}

command_volrend() {
    echo $cmd_prefix $VOLREND_BIN $threads $VOLREND_INPUTS_DIR/head $VOLREND_ARG_SIZE
}

