export PCA_PT_DIR=$PHOENIX_DIR/phoenix-2.0/tests/pca
export PCA_PT_BIN=$PCA_PT_DIR/pca-pthread

prepare_pca_pt() {
    true
}

dump_pca_pt() {
    sha1sum $PCA_PT_BIN >>$dump_program_dir/pca_pt
}

command_pca_pt() {
    echo "MR_NUMPROCS=$threads $cmd_prefix $PCA_PT_BIN -r 4000 -c 4000 >/dev/null"
}

