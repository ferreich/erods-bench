CORES=$(shell nproc)

KERNEL_VERSION=4.9.9
KERNEL_STAMP:=$(shell date +"%s.%N")

EXECUTABLES = tar curl make fakeroot make-kpkg
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH (required $(EXECUTABLES))")))

FLAVORS=vanilla wastedcores schedprofiler tptracepoint

.PHONY: all
all: ubuntu

linux-$(KERNEL_VERSION).tar.xz:
	@echo "Downloading kernel source code"
	@curl -OL 'http://kernel.org/pub/linux/kernel/v4.x/linux-$(KERNEL_VERSION).tar.xz'

SOURCES-$(KERNEL_VERSION):
	curl -L http://kernel.ubuntu.com/~kernel-ppa/mainline/v$(KERNEL_VERSION)/SOURCES -o $@

linux-$(KERNEL_VERSION)-ubuntu: linux-$(KERNEL_VERSION).tar.xz SOURCES-$(KERNEL_VERSION)
	@echo "Uncompressing source code for $@"
	@mkdir $@
	@tar xf $< -C $@ --strip-components 1
	@echo "Patching $@ with ubuntu patches"
	cd $@; for patch in $$(tail -n +2 ../SOURCES-$(KERNEL_VERSION)); do \
	    curl -OL http://kernel.ubuntu.com/~kernel-ppa/mainline/v$(KERNEL_VERSION)/$$patch; \
	    patch -p1 <$$patch; \
	done
	@cd $@; if ! test -f REPORTING-BUGS; then \
		  echo "Removed" > REPORTING-BUGS; \
	fi

linux-$(KERNEL_VERSION)-vanilla-$(KERNEL_STAMP): linux-$(KERNEL_VERSION)-ubuntu
	@echo "Copying $< for $@"
	@cp -r $< $@

linux-$(KERNEL_VERSION)-%-$(KERNEL_STAMP): linux-$(KERNEL_VERSION)-ubuntu
	@echo "Copying $< for $@"
	@cp -r $< $@
	@echo "Patching $@ with $* patches"
	@cd $@; for patch in ../$*/$(KERNEL_VERSION)/*.patch; do \
	    patch -p1 <$$patch; \
	done

linux-image-$(KERNEL_VERSION)-%-$(KERNEL_STAMP)_2_amd64.deb: linux-$(KERNEL_VERSION)-%-$(KERNEL_STAMP)
	@echo "Cleaning $<"
	make -C$< distclean
	make -C$< mrproper
	cd $<; make-kpkg clean

	@echo "Configuring $<"
	@cp /boot/config-$$(uname -r) $</.config
	make -C$< olddefconfig

	@echo "Building $<"
	cd $<; make-kpkg \
	    --rootcmd fakeroot \
	    --jobs $(CORES) \
	    --append-to-version -$*-$(KERNEL_STAMP) \
	    --revision 2 \
	    --initrd kernel_image kernel_headers

.PHONY: $(FLAVORS)
$(FLAVORS): %: linux-image-$(KERNEL_VERSION)-%-$(KERNEL_STAMP)_2_amd64.deb
